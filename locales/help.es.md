---
tags: programando-libreros, desarrollo, documentación, herramienta, orb-weaver
---

![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/Spiny_backed_orbweaver_spider.jpg/1200px-Spiny_backed_orbweaver_spider.jpg)

# Documentación sintética

orb-weaver => Implementador de Árbol de Sintaxis Abstracta. Trabaja con assets y deja a la araña tejer AST, capturar, envolver y licuar código para sitios, apps, juegos y publicaciones electrónicas.

## Siglas

* AST => [Abstract Syntax Tree] Árbol de Sintaxis Abstracta.
* SD  => [Schema Definition] Definición de Esquema.

## SD de orb-weaver

![](https://i.imgur.com/kIdYWhR.png)


* AST        => abstracción de `project` a partir de la invocación de `deployer`.
* `deployer` => sintetizador de `recipe` para generación de AST, síntesis de `object` y empaquetamiento de `output`.
* `output`   => conjunto relacionado de `object`.
* `object`   => entidad autónoma renderizable.
* `asset`    => componente para `object` implementado como `snippet`, `preset` u otro tipo de fichero.
* `snippet`  => código fuente implementable en `recipe` de `canvas` o en `var`.
* `preset`   => `snippet` primitivo proveído por `deployer`.
* `recipe`   => archivo de configuración para `project` o `canvas`.
* `project`  => `recipe` con `metas` y `packers` para al menos un `output`.
* `canvas`   => `recipe` con `metas` y `frame` para un `object`.
* `ingr`     => especificación denotada con una llave y un valor quizá configurable con `var`.
* `var`      => valor de un `ingr` para la configuración de opciones de un `snippet`.

## SD de `recipe`

* `metas`   => `ingr` de metadatos presente en `project` y `canvas`.
* `packers` => `ingr` para empaquetamiento.
* `frame`   => `ingr` de un conjunto de `snippet` y `preset` configurables con `var`.

  ! Los `metas` de `project` se escriben en `canvas` en caso de ausencia.

  ! Los `metas` de `canvas` sobreescriben los de `project` en caso de coincidencia.

  ! Los `packers` son exclusivos para `recipe` de `project`.

  ! El `frame` es exclusivo para `recipe` de `canvas`.

## SD de `var`

Dentro de `snippet`, `var` tiene esta estructura: `:opt[default]{dependencies}`.

* `opt`          => letras y dígitos ASCII arbitrarios para denotar la llave de una opción en `frame`.
* `default`      => cadena de caracteres arbitraria para indicar `val` por defecto de `opt`.
* `dependencies` => nombre de `asset` necesarios para `snippet`.

  ! `:opt[default]` => eliminación de `dependencies` en caso de descarte.

  ! `:opt[]` => vaciado para `default` en caso de descarte.

## SD de implementación de `snippet`

Dentro de `frame`, `snippet` tiene esta estructura:

```ruby
@asset += [{
  :snippet => 'type', 
  :var => {
    :opt => ['val'],
  }
}] * n
```

* `type` => tipo de `asset`.
* `:opt` => nombre de `opt` de `var` más `:` al inicio de la expresión.
* `val`  => valores coincidentes entre `:opt` y `opt`.
* `n`    => cantidad opcional de repeticiones de `asset`.

  ! La modificación del resto de los elementos de la estructura es innecesaria.

  ! `:opt` y `opt` como punto de convergencia entre SD de `var` y SD de `asset`.

## SD de `preset`

Los `preset` son estructuras primitivas con la forma `<tag>:content[]</tag>`.

* `tag` => cualquier etiqueta llamada por el usuario en `name`.

## Ejemplos de implementación de `snippet` y `preset`

### Instanciación de un `snippet`

```ruby
@asset += [{
  :snippet => 'snippet.ext',
  :var => {
    :opt1 => ['val'],
    :opt2 => ['val.html']
  }
}]
```

  ! Llamamiento de `type` a un `snippet` ubicado en el directorio de `asset`.

  ! Llamamiento de `:opt*` como `opt` de `var`.

  ! Denotación de `val` como cadena de texto o fichero externo.

### Instanciación de tres `snippet`

```ruby
@asset += [{
  :snippet => 's.e', 
  :var => {
    :opt1 => ['v1', 'v2', 'v3'],
    :opt2 => ['v1.html', 'v2.img', 'v3.mp3']
  }
}] * 3
```

  ! Implementación opcional de `n` para la repetición de `asset` en `frame`.

### Instanciación de tres `preset`

```ruby
@asset += [{
  :preset => 'name',
  :var => {
    :content => ['v1', 'v2', 'v3'],
  }
}] * 3
```

  ! Implementación similar a `snippet`, con cambio de `:snippet` por `:preset`.

  ! `:content` como opción de `:var` no configurable.

## Lista de `packers`

TODO

## Uso de orb-weaver

Llamado con `orb-weaver [ARGV]`.

### Instanciación de `recipe`

* `orb-weaver new project [name]` => instancia un `project` de orb-weaver.
* `orb-weaver new canvas [name]`  => instancia un `canvas` en `project` actual.

### Invocación de `deployer`

* `orb-weaver deploy debug`      => invoca para depuración.
* `orb-weaver deploy production` => invoca para producción.

  ! Los `snippet` no se sintetizan en modo de depuración.

  ! Los `snippet` sí se sintetizan en modo de producción.

  ! La síntesis une y minifica los `snippet` en un solo fichero.
  
### Activación de escucha

* `orb-weaver watch` => autoproduce en modo de depuración cuando detecta modificación de assets.

### Impresión de información adicional

* `orb-weaver help`    => imprime esta ayuda.
* `orb-weaver version` => imprime la versión.

:::info
* Distribuidor: Programando LIBREros
* Licencia: GPLv3
* Sitio: https://programando.li/bros/orb-weaver 
:::