$project = {
  :metas => {
    :lang => ':lang[]',
    :title => 'Title',
    :authors => ['Author 1', 'Author2'],
    :description => 'Description',
    :keywords => 'word1, word2'
  },
  :packers => [
    # WARN: delete or comment unnecessary packers
    :website => {
      :index => 'canvas',
      :assets => []
    }
  ]
}
