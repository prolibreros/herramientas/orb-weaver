# Method definition area
$canvases.push({
  :metas => {
    :id => ':id[]',
    :lang => ':lang[]',
    :title => 'Title',
    :authors => ['Author 1', 'Author 2'],
    :description => 'Description',
    :keywords => 'word1, word2',
    :favicon => '',
    :scripts => [],
    :styles => [],
    :assets => []
  },
  :frame => Class.new {@asset = Array.new
    # Waiting for assets and other fun stuff
  }.instance_variable_get('@asset')
})
