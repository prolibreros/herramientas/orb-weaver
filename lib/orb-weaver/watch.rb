require 'filewatcher'

module OrbWeaver
  class Watch

    def initialize
      prt('watch')
      Filewatcher.new(["#{$assets}/**/*.*"]).watch do |file, event|
        f = file.gsub(Dir.pwd + '/', '')

        case event
        when :created then e = :watch_gen
        when :deleted then e = :watch_del
        else e = :watch_upd
        end

        prt("\n[`#{f}`] #{I18n.t(e)}", {:string => true})
        # TODO: it should be OrbWeaver::Deploy.new('canvas') but only new canvas are being deployed
        system("orb-weaver deploy debug")
      end
    end
  end
end
