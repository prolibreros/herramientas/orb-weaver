---
tags: programando-libreros, desarrollo, todo, herramienta, orb-weaver
---

# Todo list

- [ ] Implementación de AST
- [ ] Implementación de DSL
- [ ] Metaprogramación no debería recurrir a fichero
- [ ] Instanciación de canvas no debería ser un `$canvases.push`
- [x] Autodetección de modificación de ficheros para `deploy debug`
- [x] Autodetección de ficheros en subdirectorios de `assets` sin expresar rutas
- [ ] Implementación de `deploy production`
- [x] Implementación de TTY-MD parser
- [ ] Implementación de Pandoc
- [ ] Implementación para uso con `require`
- [ ] Implementación plugin de vim
- [ ] Impresión de help == `man orb-weaver` ?
- [ ] Adición de lista de packers en `help`
- [ ] Adición epub packer
- [ ] Adición pdf packer
- [ ] Adición xml packer
- [ ] Adición jats packer
- [ ] Adición man packer
- [ ] Adición apache cordova packer
- [ ] Adición electron packer
- [ ] Adición telegram game packer
- [ ] Adición facebook game packer